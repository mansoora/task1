<!DOCTYPE html>
<html lang="en">
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
</script>
<head>

	<meta charset="utf-8" />
	<title>Task 1</title>
	<link rel="stylesheet" href="css/color.css" />
	<link rel="stylesheet" href="css/templatemo_style.css" />
	
</head>
<body>
<div class="container_24">
	<div class="grid_24 alpha omega">
			<b>TASK 1 </b>
	</div>
	<div id="mid_container_wap" class="grid_18 alpha">
			<div id="about" class="grid_18 alpha mid_container">
                            
				<div class="prefix_1 grid_16 alpha">
				
					<form id="credentials" method="post">
						<table>
							<tr>
								<td> Username: </td>
								<td> 
									<input type="text" id="user_name" name="user_name" class="textbox"/> <br />
									
								</td>
							<tr/>
							<tr>
								<td> Password:</td>
								<td> 
									<input type="password" id="pwd" name="pwd" class="textbox"/> <br/>
									
								</td>
							</tr>
							<tr>
								<td colspan=2>
									<ul >
									<li id='err_user' class="error"></li>
									<li id='err_pwd' class="error"></li>
									</ul>
									
									
								<td>
							<tr>
							<tr>
								<td ></td>
								<td><input type="submit" value="Submit" class="button" /> </td>
							</tr>
						</table>
					</form>
					
<?php
  // Check if the Page is posted 
	if($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		
		$credentials = array( 
			'username' => $_POST ['user_name'],
			'password' => $_POST ['pwd'],
			);
			
		//Code for Login
		$ch = curl_init();
		$login_URL = 'http://candidate.apiary.io/login';
		curl_setopt($ch, CURLOPT_URL, $login_URL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($credentials));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		$response = curl_exec($ch);
		
		// Convert Json into Array		
		$session_info = json_decode($response, true);
		
		//echo $session_info['session-token'];
		$SessionToken = array( 
			'session-token' => $session_info['session-token'],
			);
		// Code for Reading Notes
		$notes_URL = 'http://candidate.apiary.io/notes';
		
		curl_setopt($ch, CURLOPT_URL, $notes_URL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($SessionToken));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_POST, 0);

		$response = curl_exec($ch);

		 $notes = json_decode($response, true); 
?>
		<table>
		<th>Id</th>
		<th>Note</th>
		<?php 
			foreach ($notes as $note){
		?>
			<tr>
				<td> 
					<?php echo $note['id']; ?>
				</td>
				<td>
					<?php echo $note['title']; ?>
				</td>
			</tr>
			<?php } // End of Foreach Loop?>
		</table>
		 
	
<?php 
	
	} // closing of IF
  
?>	
				</div>
			</div>
	</div>

	<div id="footer" class="grid_24 alpha omega" style="height: 25px;">
	<div style="margin-top: 0px;">
	Made By Mansoor Ali 
	
	</div>
</div>

</div>



<script type="application/javascript">
$(document).ready(function(){
	//Validation
	$("#credentials").submit(function(e){
		$(".error").hide();
		if ( $( "#user_name" ).val().length ==0 ) {
			$("#err_user").show().html("- Please Enter Username");
			e.preventDefault();
		}
		if ($( "#pwd" ).val().length ==0 ) {
			$("#err_pwd").show().html("- Please Enter Password");
			e.preventDefault();
		}
		
	});

});
</script>

</body>

</html>